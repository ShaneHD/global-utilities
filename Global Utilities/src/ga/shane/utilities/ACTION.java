package ga.shane.utilities;

/**
 * An interface that some of my utilities use<br>
 * Has one method ({@link #run(Object...)})<br>
 * 
 * @author http://www.shane.ga
 */
public interface ACTION {
	Object run(Object... o);
}
